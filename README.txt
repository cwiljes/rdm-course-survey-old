
This dataset contains data survey that was conducted amongst participants of the course 
"Research Data Management" at Bielefeld University in the years 2013 to 2017.

The survey was done online with the Unipark/Questback system. 31 students partcipated.

The data is offered in various formats (see following section)


--------------------
FILE LIST
--------------------

00_Codebook_survey_rdm-course_Bielefeld_2018.pdf            Codebook listing the questions and system variables
01_CSV_Data_survey_rdm-course_Bielefeld_2018.csv            CSV (columns separated by character, no labels)
02_XLS_Data_survey_rdm-course_Bielefeld_2018.xls            XLS (MS Excel binary format)
03_XLSX_Data_survey_rdm-course_Bielefeld_2018.xlsx          XLSX (MS Excel Workbook) BETA
04_Triple-S_Data_survey_rdm-course_Bielefeld_2018.zip       Triple-S (XML specification file and data file)
05_Triple-S-V2_Data_survey_rdm-course_Bielefeld_2018.zip    Triple-S V2.0 Fixed Format
06_topStud_Data_survey_rdm-course_Bielefeld_2018.zip        topStud
07_XML_Data_survey_rdm-course_Bielefeld_2018.xml            XML data file
08_HTML_Data_survey_rdm-course_Bielefeld_2018.html          HTML data file
09_SPSS-binary_Data_survey_rdm-course_Bielefeld_2018.sav    SPSS (labeled data file in SPSS binary format)
10_SPSS-portable_Data_survey_rdm-course_Bielefeld_2018.por  SPSS portable file format
11_SAS_Data_survey_rdm-course_Bielefeld_2018.zip            SAS
12_Fixed-Format_Data_survey_rdm-course_Bielefeld_2018.dat   Fixed Format (e.g. to use in Quantum)
13_Quantum_Data_survey_rdm-course_Bielefeld_2018.zip        Quantum files (base file, tab file, axis file) 


--------------------
Licence
--------------------

This work is licensed under the Creative Commons Attribution 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.


--------------------
RELATED PUBLICATIONS
--------------------

For a detailed analyis, please see:

  Wiljes, C. (2018). Survey Data about Research Data Management Course. 
  Bielefeld University. doi:10.4119/unibi/2920783
  http://dx.doi.org/10.4119/unibi/2920783  

For a description of the course this data is based on, please see:

  Wiljes, C. (2018).  Results of Survey in Research Data Management Course. Bielefeld.
  PUB: https://pub.uni-bielefeld.de/publication/2920786

